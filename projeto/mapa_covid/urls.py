
import django
from django.contrib import admin
from app_mapa.views import SampleFormView
from app_mapa.views import AddInfetadoAPI
from django.urls import path

admin.autodiscover()


if django.get_version() >= '2.0.0':
    from django.urls import re_path as url
else:
    from django.conf.urls import url

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', SampleFormView.as_view()),
    path('api/addInfetado/', AddInfetadoAPI.as_view(), name='addInfetado'),
]
