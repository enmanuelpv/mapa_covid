from django.db import models
from django_google_maps.fields import AddressField, GeoLocationField
from django.utils.timezone import now

"""
    Este modelo e utilizado pelo google maps api e desta forma
    nao alterei para evitar problemas de erros no codigo
"""
class SampleModel(models.Model):
    address = AddressField(max_length=100)
    geolocation = GeoLocationField(blank=True)

    def __str__(self):
        return self.address

"""
    O modelo localizacoes representa a localizaçao de um infetado e mais os campos associados
"""
class localizacoes(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    data_infecao = models.DateField(default=now)
    data_ultimoRegisto = models.DateTimeField(auto_now=True,editable=False)
    esta_infetado = models.BooleanField(default=True)

    def __str__(self):
        return str(self.latitude) + " : " + str(self.longitude)