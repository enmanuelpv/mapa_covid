from django.contrib import admin
from django.forms.widgets import TextInput

from django_google_maps.widgets import GoogleMapsAddressWidget
from django_google_maps.fields import AddressField, GeoLocationField

from app_mapa import models

"""
    Definicao da class SampleModelAdmin. Necessário para ligar o django_admin ao google maps api
"""
class SampleModelAdmin(admin.ModelAdmin):
    formfield_overrides = {
        AddressField: {
            'widget': GoogleMapsAddressWidget
        },
        GeoLocationField: {
            'widget': TextInput(attrs={
                'readonly': 'readonly'
            })
        },
    }

"""
    Definicao da class do django admin para filtrar localizacaoes (ou infetados)
"""
class infecoesAdmin(admin.ModelAdmin):
    list_display = ('data_infecao', 'latitude', 'longitude','data_ultimoRegisto','esta_infetado')

"""
    Registar os modelos no django admin para poderem ser visualizados
"""
admin.site.register(models.SampleModel, SampleModelAdmin)
admin.site.register(models.localizacoes,infecoesAdmin)
