from rest_framework import generics, permissions
from rest_framework import serializers
from app_mapa.models import localizacoes

"""
    Serializer do infetado. Esta funcao e utilizada no webservice de adicionar Infetado.
    Basicamente a funcao transforma o modelo da localizacao e serializa
"""

class InfetadoSerializer(serializers.ModelSerializer):
    class Meta:
        model = localizacoes
        fields = ('latitude', 'longitude', 'data_infecao', 'esta_infetado')

    def create(self, validated_data):
        loc = localizacoes.objects.create(
            latitude = validated_data['latitude'], 
            longitude = validated_data['longitude'], 
            data_infecao = validated_data['data_infecao'], 
            esta_infetado = validated_data['esta_infetado'])

        return loc
